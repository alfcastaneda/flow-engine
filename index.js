import Data from './rules.json';
import chalk from 'chalk';

const Rules = Data.rules;
let example = {
  id: 1,
  name: 'Rule1',
  posts: [{
    id: 10001,
    name: 'post 1',
    pictureUrl: 'https://someurl.com/image/640x80'
  }]
}

if (process.argv[2]) {
  example = JSON.parse(process.argv[2]);
}

class Log {
  error(msg) {
    console.log(chalk.bold.red(msg));
  }
  success(msg) {
    console.log(chalk.bold.green(msg));
  }
  warning(msg) {
    console.log(chalk.orange(msg));
  }
  info(msg) {
    console.log(chalk.cyan(msg));
  }
  detail(msg) {
    console.log(chalk.bold.blue(msg));
  }

}

class FlowEngine {
  constructor(rulesSet) {
    this.appliedRules = [];
    this.rulesSet = rulesSet;
    this.log = new Log();
  }

  start(obj) {
    this.log.info('***Starting rules execution***');
    this.executeRule(this.rulesSet[0], obj);
  }

  executeRule(rule, obj) {
    let nextRuleid;
    const ruleFn = eval(`(${rule.body})`);
    const rulePassed = ruleFn(obj);
    this.log.detail(`Executing: Rule ${rule.id}:  ${rule.title}`);
    if (rulePassed) {
      this.log.success(`Rule ${rule.id} passed`);
      if (rule.true_id === null) {
        this.log.info('***Finishing rules execution***');
        return;
      }
      this.executeRule(Rules.find(r => r.id === rule.true_id), obj);
    } else {
      this.log.error(`Rule ${rule.id} failed`);
      if (rule.false_id === null) {
        this.log.info('***Finishing rules execution***');
        return;
      }
      this.executeRule(Rules.find(r => r.id === rule.false_id), obj);
    }
  }
}

const Engine = new FlowEngine(Rules);
Engine.start(example);

