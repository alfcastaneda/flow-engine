const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: './index.js',
  target: 'node',
  devtool: 'inline-source-map',
  plugins: [
    new CleanWebpackPlugin('./budle.js'),
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
    {
      test: /\.(json)$/,
      use: [
        'json-loader'
      ]
    },
    {
      test: /\.js$/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      ],
      exclude: /node_modules/,
    }]
}
};
