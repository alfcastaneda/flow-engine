# FLOW ENGINE EXECUTION
You know the drill
- git clone https://alfcastaneda@bitbucket.org/alfcastaneda/flow-engine.git
- npm install
- npm run start

You can pass a different argument as json object literal to the start command like
- npm run start '{"prop": "value"}'

And it will run the ruleset against the new value, if no value is provided will execute against common
example